﻿namespace Game_Updater
{
    partial class Main
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.PB_CLOSE = new System.Windows.Forms.PictureBox();
            this.PB_START = new System.Windows.Forms.PictureBox();
            this.PB_FC = new System.Windows.Forms.PictureBox();
            this.PB_CANCEL = new System.Windows.Forms.PictureBox();
            this.PB_FILE = new System.Windows.Forms.ProgressBar();
            this.PB_FULL = new System.Windows.Forms.ProgressBar();
            this.L_PROGRESS = new System.Windows.Forms.Label();
            this.L_PROGRESS_FILE = new System.Windows.Forms.Label();
            this.L_PROGRESS_FULL = new System.Windows.Forms.Label();
            this.BW_CRITICAL = new System.ComponentModel.BackgroundWorker();
            this.BW_FULL = new System.ComponentModel.BackgroundWorker();
            this.BW_DOWNLOAD = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.Play = new System.Windows.Forms.Label();
            this.LocalCB = new System.Windows.Forms.CheckBox();
            this.FullCheck = new System.Windows.Forms.Label();
            this.Cancel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PB_CLOSE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_START)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_FC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_CANCEL)).BeginInit();
            this.SuspendLayout();
            // 
            // PB_CLOSE
            // 
            this.PB_CLOSE.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PB_CLOSE.BackgroundImage = global::Game_Updater.Properties.Resources.close;
            this.PB_CLOSE.Image = global::Game_Updater.Properties.Resources.close;
            this.PB_CLOSE.InitialImage = global::Game_Updater.Properties.Resources.close;
            this.PB_CLOSE.Location = new System.Drawing.Point(473, 12);
            this.PB_CLOSE.Name = "PB_CLOSE";
            this.PB_CLOSE.Size = new System.Drawing.Size(20, 20);
            this.PB_CLOSE.TabIndex = 0;
            this.PB_CLOSE.TabStop = false;
            this.PB_CLOSE.Click += new System.EventHandler(this.PB_CLOSE_Click);
            this.PB_CLOSE.MouseLeave += new System.EventHandler(this.PB_CLOSE_MouseLeave);
            this.PB_CLOSE.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PB_CLOSE_MouseMove);
            // 
            // PB_START
            // 
            this.PB_START.Image = global::Game_Updater.Properties.Resources.SDisabled;
            this.PB_START.Location = new System.Drawing.Point(285, 12);
            this.PB_START.Name = "PB_START";
            this.PB_START.Size = new System.Drawing.Size(96, 99);
            this.PB_START.TabIndex = 2;
            this.PB_START.TabStop = false;
            this.PB_START.Visible = false;
            this.PB_START.Click += new System.EventHandler(this.PB_START_Click);
            this.PB_START.MouseLeave += new System.EventHandler(this.PB_START_MouseLeave);
            this.PB_START.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PB_START_MouseMove);
            // 
            // PB_FC
            // 
            this.PB_FC.Image = global::Game_Updater.Properties.Resources.FCEnabled;
            this.PB_FC.Location = new System.Drawing.Point(392, 62);
            this.PB_FC.Name = "PB_FC";
            this.PB_FC.Size = new System.Drawing.Size(101, 27);
            this.PB_FC.TabIndex = 3;
            this.PB_FC.TabStop = false;
            this.PB_FC.Visible = false;
            this.PB_FC.Click += new System.EventHandler(this.PB_FC_Click);
            this.PB_FC.MouseLeave += new System.EventHandler(this.PB_FC_MouseLeave);
            this.PB_FC.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PB_FC_MouseMove);
            // 
            // PB_CANCEL
            // 
            this.PB_CANCEL.Image = global::Game_Updater.Properties.Resources.CDisabled;
            this.PB_CANCEL.Location = new System.Drawing.Point(103, 18);
            this.PB_CANCEL.Name = "PB_CANCEL";
            this.PB_CANCEL.Size = new System.Drawing.Size(101, 27);
            this.PB_CANCEL.TabIndex = 4;
            this.PB_CANCEL.TabStop = false;
            this.PB_CANCEL.Visible = false;
            this.PB_CANCEL.Click += new System.EventHandler(this.PB_CANCEL_Click);
            this.PB_CANCEL.MouseLeave += new System.EventHandler(this.PB_CANCEL_MouseLeave);
            this.PB_CANCEL.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PB_CANCEL_MouseMove);
            // 
            // PB_FILE
            // 
            this.PB_FILE.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.PB_FILE.Location = new System.Drawing.Point(155, 130);
            this.PB_FILE.Name = "PB_FILE";
            this.PB_FILE.Size = new System.Drawing.Size(305, 10);
            this.PB_FILE.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.PB_FILE.TabIndex = 5;
            // 
            // PB_FULL
            // 
            this.PB_FULL.Location = new System.Drawing.Point(155, 146);
            this.PB_FULL.Name = "PB_FULL";
            this.PB_FULL.Size = new System.Drawing.Size(305, 10);
            this.PB_FULL.TabIndex = 6;
            // 
            // L_PROGRESS
            // 
            this.L_PROGRESS.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.L_PROGRESS.AutoEllipsis = true;
            this.L_PROGRESS.AutoSize = true;
            this.L_PROGRESS.BackColor = System.Drawing.Color.Transparent;
            this.L_PROGRESS.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.L_PROGRESS.Location = new System.Drawing.Point(57, 104);
            this.L_PROGRESS.Name = "L_PROGRESS";
            this.L_PROGRESS.Size = new System.Drawing.Size(87, 13);
            this.L_PROGRESS.TabIndex = 7;
            this.L_PROGRESS.Text = "Инициализация";
            this.L_PROGRESS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // L_PROGRESS_FILE
            // 
            this.L_PROGRESS_FILE.AutoSize = true;
            this.L_PROGRESS_FILE.BackColor = System.Drawing.Color.Transparent;
            this.L_PROGRESS_FILE.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.L_PROGRESS_FILE.Location = new System.Drawing.Point(57, 127);
            this.L_PROGRESS_FILE.Name = "L_PROGRESS_FILE";
            this.L_PROGRESS_FILE.Size = new System.Drawing.Size(92, 13);
            this.L_PROGRESS_FILE.TabIndex = 8;
            this.L_PROGRESS_FILE.Text = "Загрузка файла:";
            // 
            // L_PROGRESS_FULL
            // 
            this.L_PROGRESS_FULL.AutoSize = true;
            this.L_PROGRESS_FULL.BackColor = System.Drawing.Color.Transparent;
            this.L_PROGRESS_FULL.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.L_PROGRESS_FULL.Location = new System.Drawing.Point(57, 143);
            this.L_PROGRESS_FULL.Name = "L_PROGRESS_FULL";
            this.L_PROGRESS_FULL.Size = new System.Drawing.Size(95, 13);
            this.L_PROGRESS_FULL.TabIndex = 9;
            this.L_PROGRESS_FULL.Text = "Общий прогресс:";
            // 
            // BW_CRITICAL
            // 
            this.BW_CRITICAL.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BW_CRITICAL_DoWork);
            // 
            // BW_FULL
            // 
            this.BW_FULL.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BW_FULL_DoWork);
            // 
            // BW_DOWNLOAD
            // 
            this.BW_DOWNLOAD.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BW_DOWNLOAD_DoWork);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.Location = new System.Drawing.Point(54, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 33);
            this.label1.TabIndex = 11;
            this.label1.Text = "L24U.RU";
            // 
            // Play
            // 
            this.Play.AutoSize = true;
            this.Play.BackColor = System.Drawing.Color.Transparent;
            this.Play.Enabled = false;
            this.Play.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Play.ForeColor = System.Drawing.Color.Black;
            this.Play.Location = new System.Drawing.Point(54, 211);
            this.Play.Name = "Play";
            this.Play.Size = new System.Drawing.Size(113, 33);
            this.Play.TabIndex = 12;
            this.Play.Text = "Играть";
            this.Play.Click += new System.EventHandler(this.label2_Click_1);
            // 
            // LocalCB
            // 
            this.LocalCB.AutoSize = true;
            this.LocalCB.BackColor = System.Drawing.Color.Transparent;
            this.LocalCB.ForeColor = System.Drawing.Color.White;
            this.LocalCB.Location = new System.Drawing.Point(435, 267);
            this.LocalCB.Name = "LocalCB";
            this.LocalCB.Size = new System.Drawing.Size(58, 17);
            this.LocalCB.TabIndex = 13;
            this.LocalCB.Text = "Local?";
            this.LocalCB.UseVisualStyleBackColor = false;
            this.LocalCB.CheckedChanged += new System.EventHandler(this.LocalCB_CheckedChanged);
            // 
            // FullCheck
            // 
            this.FullCheck.AutoSize = true;
            this.FullCheck.BackColor = System.Drawing.Color.Transparent;
            this.FullCheck.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FullCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FullCheck.ForeColor = System.Drawing.Color.White;
            this.FullCheck.Location = new System.Drawing.Point(54, 169);
            this.FullCheck.Name = "FullCheck";
            this.FullCheck.Size = new System.Drawing.Size(266, 33);
            this.FullCheck.TabIndex = 14;
            this.FullCheck.Text = "Полная проверка";
            this.FullCheck.Click += new System.EventHandler(this.label3_Click);
            // 
            // Cancel
            // 
            this.Cancel.AutoSize = true;
            this.Cancel.BackColor = System.Drawing.Color.Transparent;
            this.Cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Cancel.ForeColor = System.Drawing.Color.White;
            this.Cancel.Location = new System.Drawing.Point(326, 169);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(126, 33);
            this.Cancel.TabIndex = 15;
            this.Cancel.Text = "Отмена";
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowText;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(505, 296);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.FullCheck);
            this.Controls.Add(this.LocalCB);
            this.Controls.Add(this.Play);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.L_PROGRESS_FULL);
            this.Controls.Add(this.L_PROGRESS_FILE);
            this.Controls.Add(this.L_PROGRESS);
            this.Controls.Add(this.PB_FULL);
            this.Controls.Add(this.PB_FILE);
            this.Controls.Add(this.PB_CANCEL);
            this.Controls.Add(this.PB_FC);
            this.Controls.Add(this.PB_CLOSE);
            this.Controls.Add(this.PB_START);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Автоматическое обновление клиента";
            this.TransparencyKey = this.BackColor;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Main_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Main_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.PB_CLOSE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_START)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_FC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_CANCEL)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.PictureBox PB_CLOSE;
        private System.Windows.Forms.PictureBox PB_START;
        private System.Windows.Forms.PictureBox PB_FC;
        private System.Windows.Forms.PictureBox PB_CANCEL;
        private System.Windows.Forms.ProgressBar PB_FILE;
        private System.Windows.Forms.ProgressBar PB_FULL;
        private System.Windows.Forms.Label L_PROGRESS;
        private System.Windows.Forms.Label L_PROGRESS_FILE;
        private System.Windows.Forms.Label L_PROGRESS_FULL;
        private System.ComponentModel.BackgroundWorker BW_CRITICAL;
        private System.ComponentModel.BackgroundWorker BW_FULL;
        private System.ComponentModel.BackgroundWorker BW_DOWNLOAD;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Play;
        private System.Windows.Forms.CheckBox LocalCB;
        private System.Windows.Forms.Label FullCheck;
        private System.Windows.Forms.Label Cancel;
    }
}

